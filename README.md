# SAE 401 : borneElec
## Partie frontend - react 

La partie frontend est faite en react native.

Pour accèder à la page d'accueil : [ici](https://frontendmc.mmidev.oktopod.app/borne)
La map sur cette page récuperer l'api du côté symfony grâce à ce code :


```javascript

useEffect(() => {
        fetch('https://backendmc.mmidev.oktopod.app/api/borne')
            .then(response => response.json())
            .then(setBornes)
            .catch(error => console.error('Erreur de récupération des bornes: ', error));
    }, []);

```
Grâce à la fonction setBornes, on ajoute à la variable bornes ce que retourne le lien donné sous la forme d'un tableau. Lorsque l'on clique sur un marqueur, on peut voir les détails de chaque borne présente dans la Loire.


### Interface

L'interface est responsive et possède un dark mode, cliquable dans le Header.




