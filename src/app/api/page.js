"use client";

import { useEffect, useState } from 'react';
import Header from '../../composents/Header';
import styles from '../../styles/global.scss';

export default function Api() {
    const [data, setData] = useState([]);
    const [groupedData, setGroupedData] = useState({});
    const [isLoading, setIsLoading] = useState(false); // Ajout d'un nouvel état pour le chargement
    const [selectedTable, setSelectedTable] = useState('voiture');
    const [darkMode, setDarkMode] = useState(false);

    useEffect(() => {
        const savedMode = localStorage.getItem('darkMode');
        setDarkMode(savedMode === 'true');
    }, []);

    const toggleDarkMode = (mode) => {
        setDarkMode(mode);
        localStorage.setItem('darkMode', mode);
    };

    const columnMapping = {
        voiture: { vp: 'vp', vpElec: 'vp_elec', libgeo: 'libgeo' },
        borne: { operateur: 'operateur', adresse: 'adresse', datemiseenservice: 'datemiseenservice' }
    };

    useEffect(() => {
        setIsLoading(true);
        fetch(`https://backendmc.mmidev.oktopod.app/api/${selectedTable}`)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Network response was not ok.');
            })
            .then(data => {
                setData(data);
                if (selectedTable === 'voiture') {
                    const grouped = data.reduce((acc, curr) => {
                        const libgeoValue = curr.libgeo;
                        if (!acc[libgeoValue]) {
                            acc[libgeoValue] = { ...curr, totalVp: 0, totalVpElec: 0 };
                        }
                        acc[libgeoValue].totalVp += curr.vp;
                        acc[libgeoValue].totalVpElec += curr.vpElec;
                        return acc;

                    }, {});
                    setGroupedData(grouped);
                } else if (selectedTable === 'borne') {
                    setGroupedData(data);
                }
                setIsLoading(false);
            })
            .catch(error => {
                console.error('An error has occurred: ', error);
                setIsLoading(false);
            });
    }, [selectedTable]);

    const handleTableChange = (event) => {
        setSelectedTable(event.target.value);
    };

    const renderListItems = () => {
        const columnNames = columnMapping[selectedTable];
        const isGroupedDataArray = Array.isArray(groupedData);
        const items = isGroupedDataArray ? groupedData : Object.values(groupedData);

        return items.map((item, index) => {
            if (selectedTable === 'voiture') {
                return (
                    <li key={index} className="listeData">
                        <h3>{item[columnNames.libgeo]}</h3>
                        <p>Total vpElec: {item.totalVpElec}</p>
                        <p>Total vp: {item.totalVp}</p>
                    </li>
                );
            } else if (selectedTable === 'borne') {
                return (
                    <li key={index} className="listeData">
                        <h3>{item[columnNames.operateur]}</h3>
                        <p>Adresse: {item[columnNames.adresse]}</p>
                        <p>Date de mise en service: {item[columnNames.datemiseenservice] ? new Date(item[columnNames.datemiseenservice]).toLocaleDateString() : 'N/A'}</p>
                    </li>
                );
            }
        });
    };
    return (
        <main>
            <Header toggleDarkMode={toggleDarkMode} />
            <video autoPlay loop muted className="background-video">
                <source src="/bf.mp4" type="video/mp4" />
                Votre navigateur ne supporte pas les vidéos HTML5.
            </video>
            <div className={`containerGlobal ${darkMode ? 'dark-mode' : ''}`}>
                <div className='container'>
                    <div className="divDescription">
                        Choisissez la table que vous voulez consulter :
                    </div>
                    <select className="select" value={selectedTable} onChange={handleTableChange}>
                        <option value="voiture">Voiture</option>
                        <option value="borne">Borne</option>
                    </select>

                    {isLoading ? (
                        <p>Chargement...</p>
                    ) : (
                        <ul className="ulData">
                            {renderListItems()}
                        </ul>
                    )}
                </div>

            </div>
        </main>
    );
}
