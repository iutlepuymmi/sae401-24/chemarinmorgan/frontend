"use client";

import { useParams } from "next/navigation";
import { useEffect, useState } from "react";

import Header from '../../../composents/Header';
import styles from '../../../styles/global.scss';

function Id() {
    const param = useParams()
    const [data, setData] = useState();

    const Map = dynamic(() => import('@/composents/Map'), { ssr: false });

    useEffect(() => {
        if (param.id) {
            fetch('https://backendmc.mmidev.oktopod.app/api/borne')
                .then(response => response.json())
                .then(bornes => {
                    const borneDetails = bornes.find(b => b.id.toString() === param.id.toString());
                    setData(borneDetails);
                })
                .catch(error => {
                    console.error('Erreur lors de la récupération des détails de la borne: ', error);
                });
        }
    }, []);

    const position = data ? JSON.parse(data.coordonnees).reverse() : [];

    return (
        <main>
            <Header />
            <video autoPlay loop muted className="background-video">
                <source src="/bf.mp4" type="video/mp4" />
                Votre navigateur ne supporte pas les vidéos HTML5.
            </video>
            <div className="containerGlobal">
                <div className='container'>


                    <div className="mapContainer">
                        {data && <Map position={position} zoom={13} />} {/* Utiliser les coordonnées de la borne */}
                        {data && (
                            <div>
                                <p>{data.operateur}</p>
                                <p>{data.adresse}</p>
                                <p>{data.coordonnees}</p>
                                <p>{data.datemiseenservice}</p>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </main>
    );
}

export default Id;
