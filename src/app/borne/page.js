"use client";

import React, { useEffect, useState } from 'react';
import Header from '../../composents/Header';
import dynamic from 'next/dynamic';
import styles from '../../styles/global.scss';

export default function BorneLoire() {
    const [bornes, setBornes] = useState([]);
    const [voitures, setVoitures] = useState([]);
    const [groupedVoitures, setGroupedVoitures] = useState({});
    const [selectedBorne, setSelectedBorne] = useState(null);
    const [borneDetails, setBorneDetails] = useState(null);
    const [darkMode, setDarkMode] = useState(false);

    useEffect(() => {
        const savedMode = localStorage.getItem('darkMode');
        setDarkMode(savedMode === 'true');
    }, []);

    const toggleDarkMode = (mode) => {
        setDarkMode(mode);
        localStorage.setItem('darkMode', mode);
    };

    const Map = dynamic(() => import('@/composents/Map'), { ssr: false });

    useEffect(() => {
        fetch('https://backendmc.mmidev.oktopod.app/api/borne')
            .then(response => response.json())
            .then(setBornes)
            .catch(error => console.error('Erreur de récupération des bornes: ', error));
    }, []);

    useEffect(() => {
        fetch('https://backendmc.mmidev.oktopod.app/api/voiture')
            .then(response => response.json())
            .then(voituresData => {
                const grouped = voituresData.reduce((acc, voiture) => {
                    const commune = voiture.libgeo.toUpperCase();
                    if (!acc[commune]) {
                        acc[commune] = { ...voiture, totalVp: 0, totalVpElec: 0 };
                    }
                    acc[commune].totalVp += voiture.vp;
                    acc[commune].totalVpElec += voiture.vpElec;
                    return acc;
                }, {});
                setGroupedVoitures(grouped);
            })
            .catch(error => console.error('Erreur de récupération des voitures: ', error));
    }, []);

    const handleMarkerClick = (id) => {
        const clickedBorne = bornes.find(borne => borne.id === id);
        setSelectedBorne(clickedBorne);
        setBorneDetails(groupedVoitures[clickedBorne.nomcommune.toUpperCase()]);
    };

    const markers = bornes.map(borne => {
        const coords = borne.coordonnees.replace('[', '').replace(']', '').split(',');
        const lat = parseFloat(coords[1].trim());
        const lng = parseFloat(coords[0].trim());
        const commune = borne.nomcommune.toUpperCase();
        const voitureInfo = groupedVoitures[commune];

        const detailsUrl = `/borne/${borne.id}`;

        return {
            id: borne.id,
            position: [lat, lng],
            text: voitureInfo
                ? `Opérateur: ${borne.operateur}, Adresse: ${borne.adresse}, 
                        <br><a href="${detailsUrl}">Voir détails de la borne</a>
                           <br><br><b>Voitures électriques:</b> ${voitureInfo.totalVpElec}
                           <br><b>Total voitures:</b> ${voitureInfo.totalVp}
                           `
                : `Opérateur: ${borne.operateur}, Adresse: ${borne.adresse},
                        <br><a href="${detailsUrl}">Voir détails de la borne</a><br>
                        <br><b>Voitures électriques:</b> Données manquantes<br><b>Total voitures:</b> Données manquantes
                           `
        };
    });

    return (
        <div className={`page ${darkMode ? 'dark-mode' : ''}`}>
            <main>
                <Header toggleDarkMode={toggleDarkMode} />
                <video autoPlay loop muted className="background-video">
                    <source src="/bf.mp4" type="video/mp4" />
                    Votre navigateur ne supporte pas les vidéos HTML5.
                </video>
                <div className={`containerGlobal ${darkMode ? 'dark-mode' : ''}`}>
                    <div className='container'>

                        <div className="textInfo">
                            Les voitures électriques c'est bien
                        </div>

                        <div className="divDescription">
                            Voici mon projet avec OpenData, cliquez sur n'importe quel marqueur pour avoir des informations sur une borne précise. De plus, vous pouvez voir le nombre de véhicules immatriculés par commune.
                        </div>

                        <div className="mapContainer">
                            <Map markers={markers} position={[45.489630, 4.323850]} zoom={10} onMarkerClick={handleMarkerClick} />
                            {selectedBorne && (
                                <div className={`borne-details ${selectedBorne ? 'visible' : ''}`}>
                                    <h2>{selectedBorne.operateur}</h2>
                                    <p>{selectedBorne.adresse}</p>
                                    <p>Installé le {selectedBorne.datemiseenservice}</p>
                                    <p>Coordonnées: {selectedBorne.coordonnees}</p>import React from 'react';

                                    <p>
                                        <a href={`/borne/${selectedBorne.id}`}>Voir détails de la borne (ID: {selectedBorne.id})</a>
                                    </p>
                                    {borneDetails && (
                                        <div>

                                            <p>Voitures électriques dans la commune: {borneDetails.totalVpElec}</p>
                                            <p>Total voitures dans la commune: {borneDetails.totalVp}</p>
                                        </div>
                                    )}
                                </div>
                            )}
                        </div>

                    </div>
                </div>
            </main>
        </div>
    );
}
