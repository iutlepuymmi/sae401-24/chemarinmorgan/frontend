'use client'// 45.489630, 4.32385

import React, { useEffect, useRef } from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

function Map({ markers, position, zoom, onMarkerClick }) {
    const mapRef = useRef(null);
    const markersRef = useRef({});

    useEffect(() => {
        const customIcon = L.icon({
            iconUrl: '/borneElec.png',
            iconSize: [35, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34]
        });

        if (mapRef.current === null) {
            mapRef.current = L.map('map', {
                center: position,
                zoom: zoom,
            });

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '© OpenStreetMap contributors'
            }).addTo(mapRef.current);
        }

        Object.values(markersRef.current).forEach(marker => marker.remove());
        markersRef.current = {};

        if (Array.isArray(markers)) {
            markers.forEach(({ position, text, id }) => {
                const marker = L.marker(position, { icon: customIcon })
                    .bindPopup(text)
                    .addTo(mapRef.current);

                marker.on('click', () => {
                    onMarkerClick(id);
                });

                markersRef.current[id] = marker;
            });
        } else {
            const marker = L.marker(position, { icon: customIcon })
                .addTo(mapRef.current);

            marker.on('click', () => {
                onMarkerClick(markers.id);
            });

        }
    }, [markers, position, zoom, onMarkerClick]);

    return <div id="map" style={{ height: '400px', width: '600px' }} />;
}

export default Map;





// 'use client'// 45.489630, 4.32385

// import React, { useEffect, useRef } from 'react';
// import L from 'leaflet';
// import 'leaflet/dist/leaflet.css';

// function Map({ markers, position, zoom, onMarkerClick }) {
//     const mapRef = useRef(null);
//     const markersRef = useRef({});

//     useEffect(() => {
//         const customIcon = L.icon({
//             iconUrl: '/borneElec.png',
//             iconSize: [35, 41],
//             iconAnchor: [12, 41],
//             popupAnchor: [1, -34]
//         });

//         if (mapRef.current === null) {
//             mapRef.current = L.map('map', {
//                 center: position,
//                 zoom: zoom,
//             });

//             L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//                 attribution: '© OpenStreetMap contributors'
//             }).addTo(mapRef.current);
//         }

//         Object.values(markersRef.current).forEach(marker => marker.remove());
//         markersRef.current = {};

//         if (Array.isArray(markers)) {
//             markers.forEach(({ position, text, id }) => {
//                 const marker = L.marker(position, { icon: customIcon })
//                     .bindPopup(text)
//                     .addTo(mapRef.current);

//                 marker.on('click', () => {
//                     onMarkerClick(id);
//                 });

//                 markersRef.current[id] = marker;
//             });
//         } else {
//             const marker = L.marker(position, { icon: customIcon })
//                 .addTo(mapRef.current);

//             marker.on('click', () => {
//                 onMarkerClick(markers.id);
//             });

//         }
//     }, [markers, position, zoom, onMarkerClick]);

//     return <div id="map" style={{ height: '400px', width: '600px' }} />;
// }

// export default Map;


// 'use client'// 45.489630, 4.32385

// import React, { useEffect, useRef } from 'react';
// import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
// import 'leaflet/dist/leaflet.css';

// function Map({ markers, position, zoom, onMarkerClick }) {
//     const mapRef = useRef(null);
//     const markersRef = useRef({});

//     useEffect(() => {
//         if (mapRef.current !== null) {
//             mapRef.current.setView(position, zoom);
//         }
//     }, [position, zoom]);

//     useEffect(() => {
//         if (mapRef.current !== null) {
//             markersRef.current = {};

//             if (Array.isArray(markers)) {
//                 markers.forEach(({ position, text, id }) => {
//                     const marker = (
//                         <Marker key={id} position={position} eventHandlers={{ click: () => onMarkerClick(id) }}>
//                             <Popup>{text}</Popup>
//                         </Marker>
//                     );

//                     markersRef.current[id] = marker;
//                 });
//             } else {
//                 const marker = (
//                     <Marker position={position} eventHandlers={{ click: () => onMarkerClick(markers.id) }}>
//                         <Popup>{markers.text}</Popup>
//                     </Marker>
//                 );

//                 markersRef.current[markers.id] = marker;
//             }
//         }
//     }, [markers, onMarkerClick]);

//     return (
//         <MapContainer
//             id="map"
//             center={position}
//             zoom={zoom}
//             style={{ height: '400px', width: '600px' }}
//             whenCreated={map => mapRef.current = map}
//         >
//             <TileLayer url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' attribution='© OpenStreetMap contributors' />
//             {Object.values(markersRef.current)}
//         </MapContainer>
//     );
// }

// export default Map;
