// Header.js
import React, { useState, useEffect } from 'react';
import styles from '../styles/header.scss';

function Header({ toggleDarkMode }) {
    const savedMode = localStorage.getItem('darkMode');
    const [darkMode, setDarkMode] = useState(savedMode === 'true');

    const [isMenuOpen, setIsMenuOpen] = useState(true);

    const handleDarkModeToggle = () => {
        const nouveauMode = !darkMode;
        setDarkMode(nouveauMode);
        toggleDarkMode(nouveauMode);
    };

    useEffect(() => {
        localStorage.setItem('darkMode', darkMode);
    }, [darkMode]);

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth <= 800 && setIsMenuOpen(true) ) {
                setIsMenuOpen(false);
            } 
        };

        handleResize();

        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return (
        <header className={`header ${darkMode ? 'dark-mode' : ''}`}>
            <div className="burger-menu" onClick={() => setIsMenuOpen(!isMenuOpen)}>
                <img src='/menu1.png'></img>
            </div>
            {isMenuOpen && (
                <ul className="ul">
                    <li className="li liSpe logo"><img src='/logo.png' alt="" width="80px" /></li>
                    <ul className="sousMenu">
                        <li className="li"><a href='/borne'>Utilisation</a></li>
                        <li className="li"><a href='/api'>Données</a></li>
                        <li className="li">Contact</li>
                    </ul>
                    <li className="li liSpe"></li>
                    <li className="li liSpe"></li>
                    <li className="li liSpe"></li>
                    <li className="li btnDarkMode" onClick={handleDarkModeToggle}>{darkMode ? 'Light mode' : 'Dark mode'}</li>
                </ul>
            )}
        </header>
    );
}

export default Header;
